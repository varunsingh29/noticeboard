package com.mobstac.noticeboard;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by root on 19/1/18.
 */

public class Database extends SQLiteOpenHelper {

    static final String dbname = "noticeboard";
    static final String tableName = "Notices";
    static final String col1 = "department";
    static final String col2 = "relevantFor";
    static final String col3 = "content";
    static final String col4 = "itemNo";

    public Database(Context context) {
        super(context, dbname, null,1);
    }

    public void populateDB(SQLiteDatabase db){
        ArrayList<String> items = new ArrayList<>();
        items.add("Semester Fee Notices for B.Tech Students for January Session");
        items.add("Workshop on strengthening Youth Integrity on 26th January");
        items.add("Advertisement for the post of Junior Assistant at XYZ");
        items.add("B.Tech Project Viva Schedule for all branches announced");
        items.add("Summer internship at Design Innovation Center");
        items.add("Dr. Mahitosh Mandal, SMST, elected as Fellow of Indian Academy of Sciences");
        items.add("Modern Antennas for Wireless System");
        items.add("Data Analytics with SAS");
        items.add("Developments in Pavement Engineering");
        items.add("Workshop on Safety of Women");
        // Add more notices here if required

        String[] departments = {"ALL","ALL","CSE","ALL","ECE","MECH","ECE","CSE","MECH","ALL"};
        String[] relevance = {"stud","stud","fac","all","stud","fac","all","stud","all","all"};

        Iterator it = items.iterator();
        // Insert items in Table;
        for (int i = 0; i < 10; i++){
            db.execSQL("INSERT INTO " + tableName + " values (" + i+1 + ", " + departments[i] +
            ", " + relevance[i] + ", " + it.next() + ");");
        }
        System.out.println("DB Populated");

    }
    public void printDB (){

        SQLiteDatabase db = this.getReadableDatabase();
        String tempQuery = "Select * from " + tableName;
        Cursor c = db.rawQuery(tempQuery, null);
        while (!c.isLast()){

            int sno = Integer.parseInt(c.getString(0));
            System.out.println(sno);
            String dep = c.getString(1);
            System.out.println(dep);
            String rel = c.getString(2);
            System.out.println(rel);
            String notice = c.getString(3);
            System.out.println(notice);
            c.moveToNext();
        }
        c.close();
    }
    public void onCreate (SQLiteDatabase db){
        db.execSQL("CREATE TABLE "+ tableName +" ("+col4+ " INTEGER PRIMARY KEY , "+
                col1 + " TEXT," + col2 + " TEXT," + col3 + " TEXT)");

        populateDB(db);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+tableName);
        onCreate(db);
    }

    public ArrayList<String> getDetails(int bid, String target){

        Map<Integer,String> beaconMap=new HashMap<Integer,String>();
        beaconMap.put(5959,"MECH");
        beaconMap.put(9741,"CSE");
        beaconMap.put(1111, "ECE");   // Enter valid BID here

        SQLiteDatabase db = this.getReadableDatabase();
        String tempQuery = ("SELECT " + col3 + " from " + tableName + " where " +
        col1 + " = " + beaconMap.get(bid) + " and " + col2 + " = " + target + " ;");
        System.out.println("View Query is : " + tempQuery);
        Cursor c = db.rawQuery(tempQuery, null);

        ArrayList<String> result = new ArrayList<>();
        while (c.getCount() != 0 && !c.isLast()){
            result.add(c.getString(0));
            c.moveToNext();
        }
        c.close();
        return(result);
    }
}
